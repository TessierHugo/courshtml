<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title> Cours html </title>
        <meta charset="UTF-8">
        <!--  <link rel="stylesheet" href="css/Index.css" />-->
        <link rel="stylesheet"  href="css/Index.bis.css"/>
    </head>
    <body>
        <header>
            <h1>
                TITRE
            </h1>
            <img src="Images/Latin_H.svg.png" alt="Lettre H" />
            <nav>
                <ul>
                    <li>
                        <a href="vue/sDetails.html" target="_blank"> sDetails </a></li><!--
                    --><li>

                        <a href="vue/sArticle.html" target="_blank"> sArticles </a>
                        <ul>
                            <li><a href="">WWWWW</a></li>
                            <li><a href="">www www www www </a></li>
                            <li><a href="">WWWWW</a></li>
                        </ul><!--
                        --><li><a href="vue/sListe.html" target="_blank"> sListe </a></li><!--
                    --><li><a href="vue/sTableaux.html" target="_blank"> sTableaux </a></li>
                </ul>
            </nav>
        </header>
        <main>
            <ul>
                <li> A </li>
                <li> B </li>
                <li>  C
                    <ul>
                        <li> D </li>
                        <li> E</li>
                        <li> F <ul>
                                <li> G </li>
                                <li> H </li>
                                <li> I </li>
                            </ul> </li>
                    </ul> </li>
            </ul>
            <div class="conteneur">
                <div class="produit"></div>
                <div class="produit"></div>
                <div class="produit"></div>
                <div class="produit"></div>
                <div class="produit"></div>
                <div class="produit"></div>
                <div class="produit"></div>
                <div class="produit"></div>
                <div class="produit"></div>
                <div class="produit"></div>
                <div class="produit"></div>
                <div class="produit"></div>
                <div class="produit"></div>
                <div class="produit"></div>
                <div class="produit"></div>
                <div class="produit"></div>
                <div class="produit"></div>
                <div class="produit"></div>
                <div class="produit"></div>
                <div class="produit"></div>
                <div class="produit"></div>
                <div class="produit"></div>
            </div>

            <h2>
                TITRE
            </h2>
            <p>

                Lorem ipsum <a href="">dolor</a> sit amet, consectetur adipiscing elit. Integer pharetra sem ac nulla tristique bibendum. Phasellus sit amet libero sit amet libero rutrum facilisis a nec nunc. Sed at convallis dui. Nunc id feugiat lectus. Praesent iaculis libero faucibus sodales elementum. Mauris quis scelerisque tellus. Mauris pretium mauris sem, in consectetur diam feugiat sit amet.
            </p>
            <p>
                Phasellus feugiat nisl non augue faucibus ornare. Mauris quis magna id arcu porttitor dictum nec sed dolor. Curabitur maximus dui urna, eget venenatis lorem pellentesque sed. Integer sit amet tortor eget velit fringilla tristique sed ut sem. Proin nunc turpis, pulvinar vitae commodo sit amet, ultrices nec lacus. Proin vulputate pretium justo, eu posuere enim hendrerit vel. Phasellus pulvinar vehicula lacus, quis hendrerit arcu elementum vel. Cras non justo at nulla vehicula pellentesque. Proin elementum mauris sit amet pharetra malesuada. Donec aliquam magna quis turpis elementum iaculis. Ut vitae quam ex. Praesent blandit diam non est dictum, sit amet interdum quam sagittis.

            </p>

            <p>
                Duis tempus risus erat, vitae suscipit velit accumsan et. Sed sit amet orci mauris. Aliquam erat volutpat. Phasellus hendrerit dignissim diam id varius. Morbi sapien augue, tempus sed pharetra viverra, sollicitudin eu mauris. Sed quis egestas ex, sit amet mattis dui. Aenean placerat sodales viverra. Aliquam enim ex, suscipit in mauris sed, accumsan rutrum tellus. Donec sed risus dui. Sed sit amet convallis sem, quis pellentesque sem. Phasellus quam quam, bibendum nec nulla ac, auctor varius velit.
            </p>

            <p>
                Vivamus sit amet pharetra justo. Morbi vel odio metus. Nunc sed turpis ac ex convallis porta. Integer efficitur nec lorem viverra dictum. Integer volutpat consectetur elit vel feugiat. Cras convallis id eros et placerat. Sed aliquet urna metus, a viverra augue venenatis et. Ut tortor nisl, aliquet et arcu vitae, vulputate malesuada enim. Suspendisse tempus libero a ligula accumsan, ut tristique urna pharetra. In hac habitasse platea dictumst. Duis a auctor turpis. Mauris dignissim tempor ante tempus vehicula. Vestibulum nec porttitor ligula, ac finibus nunc. Maecenas vitae quam molestie, commodo ex ac, egestas mauris. Sed efficitur eros magna, eget venenatis orci euismod non. Aenean sapien tortor, ultrices eget velit quis, condimentum feugiat dui.
            </p>

            <p>
                Vivamus sed imperdiet nisi. In bibendum lacus id orci euismod, ac feugiat orci laoreet. Donec vel semper ante, lacinia pulvinar orci. Suspendisse ipsum enim, venenatis eget porttitor tincidunt, scelerisque eget nulla. Etiam ut ipsum blandit, convallis elit sed, eleifend nisi. Etiam vitae varius mauris. Donec lobortis sem libero, id pretium justo consequat in. Aenean venenatis, eros vitae interdum ornare, orci dolor porttitor nisl, at elementum mauris quam vitae augue. Praesent venenatis mi vitae congue finibus. Curabitur sollicitudin, nibh eget lacinia rutrum, leo odio elementum augue, vitae vulputate ante nulla malesuada urna. Integer congue metus sed dui vehicula suscipit. Nunc id finibus libero. Suspendisse sed lacinia quam. Morbi eget elit in arcu varius mattis in vestibulum justo. Aenean convallis quam et risus ultrices, sit amet tempor neque dapibus.
            </p>

        </main>
        <footer>
            contact
        </footer>

    </body>
</html>
